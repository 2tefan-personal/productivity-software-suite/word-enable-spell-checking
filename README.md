# *Word* - Enable spellchecking

- [English 🇬🇧/🇺🇸](#english-🇬🇧🇺🇸)
- [Deutsch 🇦🇹/🇩🇪/🇨🇭](#deutsch-🇦🇹🇩🇪🇨🇭)


# English 🇬🇧/🇺🇸
## Open word
<img src="src/01_open_word.png" alt="Screenshot of word opening" width="400"/>

## Open Language Preferences (under Review)
<img src="src/02_open_language_preferences.png" alt="Screenshot of going to tab 'Review', selecting 'Language' and then 'Language Preferences...'" width="400"/>

## Select Proofing
<img src="src/03_navigate_to_proofing.png" alt="Screenshot of selecting 'Proofing' on the left side" height="400"/>

## Enable spell checking
<img src="src/04_enable_spell_checking.png" alt="Screenshot of spell checking enabled. Select 'Check spelling as you type', 'Mark grammar errors as you type', 'Frequently confused words' aand 'Check grammar and refinements in the Editor Pane'." width="400"/>


# Deutsch 🇦🇹/🇩🇪/🇨🇭

## Word öffnen
<img src="src/01_open_word.png" alt="Screenshot von Word, wie es sich öffnet." width="400"/>

## Spracheinstellungen öffnen (unter Überprüfen)
<img src="src/02_open_language_preferences_ger.png" alt="Screenshot von Auswahl von 'Überprüfen' als Tab, dann 'Sprache', noch einmal 'Sprache' und dann 'Spracheinstellungen...'" width="400"/>

## Dokumentprüfung auswählen
<img src="src/03_navigate_to_proofing_ger.png" alt="Screenshot von Auswahl von 'Dokumentprüfung' auf der linken Seite" height="400"/>

## Rechtschreibüberprüfung auswählen
<img src="src/04_enable_spell_checking_ger.png" alt="Screenshot von der aktivierten Rechtschreibüberprüfung. Hierzu 'Rechtschreibung während der Eingabe überprüfen', 'Grammatikfehler w#hrend der Eingabe markieren', 'Häufig verwechselte Wörter', und 'Grammatik und Verbesserungen im Editor-Bereich überprüfen' auswählen." width="400"/>
